﻿Imports System.IO
Imports System.Net
Imports System.Reflection

Public Class Form1
    Public Const ROCK As String = "Rock"
    Public Const PAPER As String = "Paper"
    Public Const SCISSORS As String = "Scissors"
    ' player info
    Public Wins As Integer = 0
    Public Draws As Integer = 0
    Public Losses As Integer = 0
    Public ReadOnly Property lastPlayerMove As String
        Get
            If PreviousPlayerMoves.Count <= 0 Then
                PreviousPlayerMoves.Add(ROCK)
            End If
            Return PreviousPlayerMoves.Item(PreviousPlayerMoves.Count - 1)
        End Get
    End Property
    Public PreviousPlayerMoves As New List(Of String)
    Public ReadOnly Property PlayerMostOftenAllTime As String
        Get
            Dim rockAmnt As Integer = 0
            Dim paperAmnt As Integer = 0
            Dim scisAmnt As Integer = 0
            For Each move As String In PreviousPlayerMoves
                If move = ROCK Then rockAmnt += 1
                If move = PAPER Then paperAmnt += 1
                If move = SCISSORS Then scisAmnt += 1
            Next
            If rockAmnt > paperAmnt And rockAmnt > scisAmnt Then Return ROCK
            If paperAmnt > rockAmnt And rockAmnt > scisAmnt Then Return PAPER
            Return SCISSORS ' otherwise, scissors
        End Get
    End Property
    Dim rnd As New Random()
    Public Function WhatBeatsThis(move As String) As String
        If move = ROCK Then
            Return PAPER
        ElseIf move = PAPER Then
            Return SCISSORS
        ElseIf move = SCISSORS Then
            Return ROCK
        Else
            Return "That move.. should not happen"
        End If
    End Function
    Public Function GetRandomMove() As String
        Dim i As Integer = rnd.Next(0, 3)
        If i = 0 Then
            Return ROCK
        ElseIf i = 1 Then
            Return PAPER
        Else
            Return SCISSORS
        End If
    End Function
    Public Function DoesFirstWin(move1 As String, move2 As String) As Boolean
        Dim beater As String = WhatBeatsThis(move1)
        Return Not beater = move2
    End Function
    Public Function DoesFirstDraw(move1 As String, move2 As String) As Boolean
        Return move1 = move2
    End Function
    Public Function GetNextCompHand() As String
        Dim randomInt As Integer = rnd.Next(0, 10)
        If randomInt >= 8 Then
            ' try to beat their last move
            If Not String.IsNullOrWhiteSpace(lastPlayerMove) Then Return WhatBeatsThis(lastPlayerMove)
        ElseIf randomInt >= 6 Then
            ' try to beat their most commonly used move
            If Not String.IsNullOrWhiteSpace(lastPlayerMove) Then Return WhatBeatsThis(PlayerMostOftenAllTime)
        End If
        Return GetRandomMove()
    End Function
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lbl_playerChose.Hide()
        lbl_CompChose.Hide()
        lbl_winMsg.Hide()
        HandleVersion()
    End Sub

    Public Sub HandleMove(player As String, comp As String)
        lbl_playerChose.Show()
        lbl_CompChose.Show()
        lbl_winMsg.Show()
        Dim playerWins As Boolean = DoesFirstWin(player, comp)
        Dim disMessage As String = "I'm not sure.."
        If playerWins = False Then
            Losses += 1
            disMessage = "You lose!"
        Else
            Dim draw As Boolean = DoesFirstDraw(player, comp)
            If draw = True Then
                Draws += 1
                disMessage = "It's a draw!"
            Else
                Wins += 1
                disMessage = "You win!"
            End If
        End If
        PreviousPlayerMoves.Add(player)
        lbl_score.Text = "Wins: " & Wins.ToString() & " | Losses: " & Losses.ToString() & " | Draws: " & Draws.ToString()
        lbl_CompChose.Text = "I pick: " & comp
        lbl_playerChose.Text = "You pick: " & player
        lbl_winMsg.Text = disMessage
    End Sub

    Private Sub pb_Rock_Click(sender As Object, e As EventArgs) Handles pb_Rock.Click
        Dim compHand As String = GetNextCompHand()
        Dim playerHand As String = ROCK
        HandleMove(playerHand, compHand)
    End Sub

    Private Sub pb_Paper_Click(sender As Object, e As EventArgs) Handles pb_Paper.Click
        Dim compHand As String = GetNextCompHand()
        Dim playerHand As String = PAPER
        HandleMove(playerHand, compHand)
    End Sub

    Private Sub pb_Scissors_Click(sender As Object, e As EventArgs) Handles pb_Scissors.Click
        Dim compHand As String = GetNextCompHand()
        Dim playerHand As String = SCISSORS
        HandleMove(playerHand, compHand)
    End Sub

    Public Function GetLatestVersion(url As String) As Version
        Try
            Dim myReq As HttpWebRequest = WebRequest.Create(url)
            Dim response As HttpWebResponse = myReq.GetResponse()
            Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
            Dim strResponse As String = resStream.ReadToEnd().ToString()
            Dim location As Integer = strResponse.IndexOf("<Assembly: AssemblyFileVersion(")
            If location >= 0 Then
                Dim getVersion As String = strResponse.Substring(location + "<Assembly: AssemblyFileVersion(".Length + 1)
                getVersion = getVersion.Substring(0, getVersion.IndexOf(ControlChars.Quote))
                Dim latest As Version = New Version(getVersion)
                Return latest
            Else
                Return New Version("0.0.0.0")
            End If
        Catch ex As Exception
            MsgBox(ex.ToString())
        End Try
        Return New Version("0.0.0.0")
    End Function
    Const clName As String = "RPS"
    Dim LatestVersion As Version = New Version("0.0.0.0")
    Dim ThisThingVersion As New Version("0.0.0.0")
    Public Sub HandleVersion()
        LatestVersion = GetLatestVersion("https://bitbucket.org/thegrandcoding/rockpaperscissors/raw/HEAD/RockPaperScissors/My%20Project/AssemblyInfo.vb")
        ThisThingVersion = Assembly.GetExecutingAssembly().GetName().Version
        Me.Text += " | " & ThisThingVersion.ToString()
        If ThisThingVersion.CompareTo(LatestVersion) = 0 Then
            Me.Text += " (UpToDate)"
        ElseIf ThisThingVersion.CompareTo(LatestVersion) = 1 Then
            Me.Text += " (Ahead)"
        Else
            Me.Text += " (OutOfDate)"
            MsgBox("Warning:" & vbCrLf & "This " & clName & " is not running the latest version" & vbCrLf & "Your version: " & ThisThingVersion.ToString() & vbCrLf & "Latest Version: " & LatestVersion.ToString() & vbCrLf & "We will now attempt to download the latest version")
            Dim url As String = "https://bitbucket.org/thegrandcoding/rockpaperscissors/raw/HEAD/RockPaperScissors/bin/Debug/RockPaperScissors.exe"
            Dim newName As String = "new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe"
            Try
                Dim web_Download As New WebClient
                If File.Exists(newName) Then
                    Dim dlVersion As Version = New Version(FileVersionInfo.GetVersionInfo(newName).FileVersion)
                    If Not dlVersion = LatestVersion Then
                        File.Delete(newName) ' not the latest version
                        web_Download.DownloadFile(url, newName)
                    End If
                Else
                    web_Download.DownloadFile(url, newName)
                End If
                MsgBox("New " & clName & " has been downloaded. Checking version...") ' we dont tell them of the old version.
                Dim downloadVersion As Version = New Version(FileVersionInfo.GetVersionInfo(newName).FileVersion)
                If downloadVersion = LatestVersion Then
                    MsgBox("Version is valid. Running new client now.. this client will close.")
                    Process.Start(newName)
                    Threading.Thread.Sleep(5)
                    End
                Else
                    MsgBox("Attempts to download the new version have failed." & vbCrLf & "Please download manually.")
                End If
            Catch ex As UnauthorizedAccessException
                MsgBox("You do not have permissions to download/write at that folder" & vbCrLf & "Please copy this client to your personal drive, or run as administrator, and try again.")
            Catch ex As WebException
                MsgBox("You do not have permissions to download/write at that folder" & vbCrLf & "Please copy this client to your personal drive, or run as administrator, and try again.")
            Catch ex As Exception
                MsgBox(ex.ToString())
            Finally
                MsgBox("We will now close.")
                End
            End Try
        End If
        If IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath).ToString = "new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe" Then
            MsgBox("WARNING:" & vbCrLf & "It is advised that you download the complete Visual Studio project of the client." & vbCrLf & "You may ignore this message.")
        End If
        ' todo:
        ' get latest version from bitbucket - DONE
        ' get latest version of client - DONE
        ' download newest client if this one is old. - DONE
        ' run that client and close this one. - DONE
        ' also check for no permissions - DONE
    End Sub
End Class
