﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.lbl_header = New System.Windows.Forms.Label()
        Me.lbl_score = New System.Windows.Forms.Label()
        Me.pb_Rock = New System.Windows.Forms.PictureBox()
        Me.pb_Paper = New System.Windows.Forms.PictureBox()
        Me.pb_Scissors = New System.Windows.Forms.PictureBox()
        Me.lbl_helpMsg = New System.Windows.Forms.Label()
        Me.lbl_playerChose = New System.Windows.Forms.Label()
        Me.lbl_CompChose = New System.Windows.Forms.Label()
        Me.lbl_winMsg = New System.Windows.Forms.Label()
        CType(Me.pb_Rock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb_Paper, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb_Scissors, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_header
        '
        Me.lbl_header.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_header.Location = New System.Drawing.Point(12, 9)
        Me.lbl_header.Name = "lbl_header"
        Me.lbl_header.Size = New System.Drawing.Size(630, 43)
        Me.lbl_header.TabIndex = 0
        Me.lbl_header.Text = "Rock, Paper, Scissors"
        Me.lbl_header.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_score
        '
        Me.lbl_score.Location = New System.Drawing.Point(12, 52)
        Me.lbl_score.Name = "lbl_score"
        Me.lbl_score.Size = New System.Drawing.Size(630, 23)
        Me.lbl_score.TabIndex = 1
        Me.lbl_score.Text = "Wins: 0 | Losses: 0 | Draws: 0"
        Me.lbl_score.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pb_Rock
        '
        Me.pb_Rock.Image = CType(resources.GetObject("pb_Rock.Image"), System.Drawing.Image)
        Me.pb_Rock.InitialImage = CType(resources.GetObject("pb_Rock.InitialImage"), System.Drawing.Image)
        Me.pb_Rock.Location = New System.Drawing.Point(13, 145)
        Me.pb_Rock.Name = "pb_Rock"
        Me.pb_Rock.Size = New System.Drawing.Size(160, 120)
        Me.pb_Rock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pb_Rock.TabIndex = 2
        Me.pb_Rock.TabStop = False
        '
        'pb_Paper
        '
        Me.pb_Paper.Image = CType(resources.GetObject("pb_Paper.Image"), System.Drawing.Image)
        Me.pb_Paper.Location = New System.Drawing.Point(249, 145)
        Me.pb_Paper.Name = "pb_Paper"
        Me.pb_Paper.Size = New System.Drawing.Size(160, 120)
        Me.pb_Paper.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pb_Paper.TabIndex = 3
        Me.pb_Paper.TabStop = False
        '
        'pb_Scissors
        '
        Me.pb_Scissors.Image = CType(resources.GetObject("pb_Scissors.Image"), System.Drawing.Image)
        Me.pb_Scissors.Location = New System.Drawing.Point(483, 145)
        Me.pb_Scissors.Name = "pb_Scissors"
        Me.pb_Scissors.Size = New System.Drawing.Size(160, 120)
        Me.pb_Scissors.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pb_Scissors.TabIndex = 4
        Me.pb_Scissors.TabStop = False
        '
        'lbl_helpMsg
        '
        Me.lbl_helpMsg.Location = New System.Drawing.Point(14, 289)
        Me.lbl_helpMsg.Name = "lbl_helpMsg"
        Me.lbl_helpMsg.Size = New System.Drawing.Size(630, 23)
        Me.lbl_helpMsg.TabIndex = 5
        Me.lbl_helpMsg.Text = "Please click any one of the above Rock-Paper-Scissors to start a game"
        Me.lbl_helpMsg.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbl_playerChose
        '
        Me.lbl_playerChose.Location = New System.Drawing.Point(14, 344)
        Me.lbl_playerChose.Name = "lbl_playerChose"
        Me.lbl_playerChose.Size = New System.Drawing.Size(630, 23)
        Me.lbl_playerChose.TabIndex = 6
        Me.lbl_playerChose.Text = "You picked: "
        Me.lbl_playerChose.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbl_CompChose
        '
        Me.lbl_CompChose.Location = New System.Drawing.Point(18, 367)
        Me.lbl_CompChose.Name = "lbl_CompChose"
        Me.lbl_CompChose.Size = New System.Drawing.Size(630, 23)
        Me.lbl_CompChose.TabIndex = 7
        Me.lbl_CompChose.Text = "I picked:"
        Me.lbl_CompChose.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbl_winMsg
        '
        Me.lbl_winMsg.Location = New System.Drawing.Point(18, 390)
        Me.lbl_winMsg.Name = "lbl_winMsg"
        Me.lbl_winMsg.Size = New System.Drawing.Size(630, 23)
        Me.lbl_winMsg.TabIndex = 8
        Me.lbl_winMsg.Text = "You lose! / You win!"
        Me.lbl_winMsg.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(654, 436)
        Me.Controls.Add(Me.lbl_winMsg)
        Me.Controls.Add(Me.lbl_CompChose)
        Me.Controls.Add(Me.lbl_playerChose)
        Me.Controls.Add(Me.lbl_helpMsg)
        Me.Controls.Add(Me.pb_Scissors)
        Me.Controls.Add(Me.pb_Paper)
        Me.Controls.Add(Me.pb_Rock)
        Me.Controls.Add(Me.lbl_score)
        Me.Controls.Add(Me.lbl_header)
        Me.Name = "Form1"
        Me.Text = "Rock Paper Scissors"
        CType(Me.pb_Rock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb_Paper, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb_Scissors, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lbl_header As Label
    Friend WithEvents lbl_score As Label
    Friend WithEvents pb_Rock As PictureBox
    Friend WithEvents pb_Paper As PictureBox
    Friend WithEvents pb_Scissors As PictureBox
    Friend WithEvents lbl_helpMsg As Label
    Friend WithEvents lbl_playerChose As Label
    Friend WithEvents lbl_CompChose As Label
    Friend WithEvents lbl_winMsg As Label
End Class
